import React, {useState} from "react";
import PropTypes from "prop-types";
import "./login.css";

async function loginUser(username, password) {
    return fetch('http://localhost:8080/user/getUserByName/'+username, {
        method: 'GET',
        headers: {
            'Authorization': 'Basic '+btoa(username+":"+password),
            'Content-Type': 'application/json'
        },
    })
        .then(data => data.json())
}

async function listUser() {
    return fetch('http://localhost:8080/user', {
        method: 'GET',
        headers: {
            'Authorization': 'Basic '+btoa("norvin:testtesttest"),
            'Content-Type': 'application/json'
        },
    })
        .then(data => data.json())
        .then(data => console.log(data))
}


function minimizeWindow(e) {
    alert("Minimize!");
}

function maximizeWindow(e) {
    alert("Maximize!");
}

function closeWindow(e) {
    alert("Close!");
}

export default function Login({setToken}) {
    const [username, setUserName] = useState();
    const [password, setPassword] = useState();

    const handleSubmit = async e => {
        e.preventDefault();
        const token = await loginUser(username, password);
        console.log(token)
        setToken(token);
    }

    return(
        <div className="login-wrapper">
            <h1>Login</h1>
            <form onSubmit={handleSubmit}>
                <label>
                    <p>Username</p>
                    <input type="text" onChange={e => setUserName(e.target.value)}/>
                </label>
                <label>
                    <p>Password</p>
                    <input type="password" onChange={e => setPassword(e.target.value)}/>
                </label>
                <div>
                    <button type="submit">Submit</button>
                </div>
            </form>
        </div>
    )
}

Login.propTypes = {
    setToken: PropTypes.func.isRequired
}
