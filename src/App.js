import logo from './logo.svg';
import './App.css';
import './components/login.css'
import './components/home.css'
import React, {useState} from 'react';
import {HashRouter,Link,Route,Switch} from "react-router-dom";
import Login from "./components/login";
import Profil from "./components/profil";
import Home from "./components/home";
import UserBib from "./components/userbib";
import Bibs from "./components/bibs";


function minimizeWindow(e) {
    alert("Minimize!");
}

function maximizeWindow(e) {
    alert("Maximize!");
}

function closeWindow(e) {
    alert("Close!");
}

let initalState = {
    //mit reducer
}

function App() {
    const [token, setToken] = useState();
    if(!token) {
        return <Login setToken={setToken} />
    }

    return (
        <div className="all-wrapper">
            <div className="test">
            </div>
            <HashRouter>
                <div className="App">
                    <div className="App-menu">
                        <div className="App-header">
                            <img src={logo} className="App-logo" alt="logo" />
                            <h2>ourbook</h2>
                        </div>
                        <Link to="/" className="App-link"><h2 className="linktext">home</h2></Link>
                        <Link to="/wishlist" className="App-link"><h2 className="linktext">wishlist</h2></Link>
                        <Link to="/bibs" className="App-link"><h2 className="linktext">bibs</h2></Link>
                        <Link to="/ausleihliste" className="App-link"><h2 className="linktext">ausleihliste</h2></Link>
                        <Link to="/profil" className="App-link"><h2 className="linktext">profil</h2></Link>
                    </div>
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route exact path="/wishlist" component={UserBib}/>
                        <Route exact path="/bibs" component={Bibs}/>
                        <Route exact path="/ausleihliste" component={UserBib}/>
                        <Route exact path="/profil" component={Profil}/>
                    </Switch>
                </div>
            </HashRouter>
        </div>
    );
}

export default App;
